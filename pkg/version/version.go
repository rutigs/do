package version

// Version - version number for this application
// should follow semver
var Version = "v0.1.0"

// GitCommit - current commit of this running application
// will be injected at compile time
var GitCommit = ""
