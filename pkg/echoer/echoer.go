package echoer

import (
	"github.com/tidwall/gjson"
	"github.com/tidwall/sjson"
)

// ContainsEchoed - return bool value for json contains `"echoed":true`
func ContainsEchoed(jsonString string) bool {
	parsedJson := gjson.Get(jsonString, "echoed")

	if !parsedJson.Exists() {
		return false
	}

	if !parsedJson.Bool() {
		return false
	}

	return true
}

// AddEchoed - add `"echoed":true` to a json string and return it
func AddEchoed(jsonString string) string {
	// sjson.Set returns an error if the path is invalid so we can ignore it
	value, _ := sjson.Set(jsonString, "echoed", true)
	return value
}
