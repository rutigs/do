package echoer

import (
	"testing"
)

// TestContainsEchoed - test the ContainsEchoed function
func TestContainsEchoed(t *testing.T) {
	type testCase struct {
		input string
		want  bool
	}

	tests := []testCase{
		// Contains proper echoed
		{`{"something":"else","echoed":true}`, true},
		// Contains echoed but not correct boolean
		{`{"something":"else","echoed":false}`, false},
		// Does not contain echoed
		{`{"something":"else"}`, false},
		// Contains echo but not a top level
		{`{"something":{"echoed":true}}`, false},
	}

	for i := 0; i < len(tests); i++ {
		test := tests[i]

		got := ContainsEchoed(test.input)
		if got != test.want {
			t.Errorf("%d: ContainsEchoed(%s) = %v, Want = %v", i, test.input, got, test.want)
		}
	}
}

// TestAddEchoed - test the AddEchoed function
func TestAddEchoed(t *testing.T) {
	type testCase struct {
		input string
		want  string
	}

	tests := []testCase{
		// Adds entire key->value
		{`{"something":"else"}`, `{"something":"else","echoed":true}`},
		// Updates boolean
		{`{"something":"else","echoed":false}`, `{"something":"else","echoed":true}`},
	}

	for i := 0; i < len(tests); i++ {
		test := tests[i]

		got := AddEchoed(test.input)
		if got != test.want {
			t.Errorf("%d: AddEchoed(%s) = '%v', Want = '%v'", i, test.input, got, test.want)
		}
	}
}
