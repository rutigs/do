package prometheus

import (
	"net/http"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var (
	// EchoCount - Prometheus counter for echo requests
	EchoCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "echo_server_echo_total",
			Help: "Total number of echo'd requests",
		},
		[]string{"status"},
	)
)

// init - called on package initialization to ensure Prometheus registers our custom metric
func init() {
	prometheus.MustRegister(EchoCount)
}

// PrometheusHandler - return the prometheus handler for /metrics
func PrometheusHandler() http.Handler {
	return promhttp.HandlerFor(
		prometheus.DefaultGatherer,
		promhttp.HandlerOpts{
			EnableOpenMetrics: true,
		},
	)
}
