##############
# CI
##############

VERSION    := $(shell grep 'Version' pkg/version/version.go | awk '{ print $$4 }' | tail -n1 | tr -d '"')
GIT_COMMIT := $(shell git rev-parse --short HEAD)

DOCKER_TAG := rutigs/do:$(VERSION)-$(GIT_COMMIT)

tag:
	echo $(DOCKER_TAG) > DOCKER_TAG

##############
# Build
##############

run:
	go run main.go

build:
	go build \
		-ldflags "-X gitlab.com/rutigs/do/pkg/version.GitCommit=$(GIT_COMMIT)" \
		-o bin/do *.go

linux:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build \
		-ldflags "-X gitlab.com/rutigs/do/pkg/version.GitCommit=$(GIT_COMMIT)" \
		-o bin/do *.go

docker:
	docker build . -t $(DOCKER_TAG)

docker-run:
	docker run --rm -p 3000:3000 -t $(DOCKER_TAG)

##############
# Test
##############

.PHONY: test
test: vet unit

.PHONY: unit
unit: 
	go test -v ./...

.PHONY: vet
vet:
	go mod tidy
	go fmt ./...
	go vet ./...

# Works local but installing gotools in CI can be annoying
# Can add this back to the vet stage if we have time
# golint ./...
