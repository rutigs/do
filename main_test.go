package main

import (
	"bytes"
	"io/ioutil"
	"net/http/httptest"
	"testing"

	fiber "github.com/gofiber/fiber/v2"
	"gitlab.com/rutigs/do/pkg/echoer"
)

// TestEchoHandler
func TestEchoHandler(t *testing.T) {
	app := setupServer()

	// 1. Valid request to echo json
	req := httptest.NewRequest("POST", "http://localhost:3000/api/echo", bytes.NewReader([]byte(`{"something":"to-echo"}`)))
	req.Header.Set("Content-Type", "application/json")

	resp, _ := app.Test(req)

	if resp.StatusCode == fiber.StatusOK {
		respBody, _ := ioutil.ReadAll(resp.Body)
		if !echoer.ContainsEchoed(string(respBody)) {
			t.Errorf("Error during valid echo")
		}
	}

	// 2. Invalid request via invalid json - missing }
	req = httptest.NewRequest("POST", "http://localhost:3000/api/echo", bytes.NewReader([]byte(`{"something":"to-echo"`)))
	req.Header.Set("Content-Type", "application/json")

	resp, _ = app.Test(req)

	if resp.StatusCode != fiber.StatusBadRequest {
		t.Errorf("Error: echo request should be invalid json")
	}

	// 3. Invalid request already contains echoed
	req = httptest.NewRequest("POST", "http://localhost:3000/api/echo", bytes.NewReader([]byte(`{"something":"to-echo","echoed":true}`)))
	req.Header.Set("Content-Type", "application/json")

	resp, _ = app.Test(req)

	if resp.StatusCode != fiber.StatusBadRequest {
		t.Errorf("Error: echo request is already echoed")
	}
}
