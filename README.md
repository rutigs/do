# Echo Server

Echo server is an application that echo's json that is requested against it.

## Overview

The application is written in Golang using the [Fiber framework](https://github.com/gofiber/fiber). The application has two endpoints:

```
/api/echo  # Accepts POST/PUT requests with json bodies to echo
/metrics   # Accepts GET requests and returns prometheus metrics
```

## How To Run It

A Makefile with several targets has been provided for convenience and for consistency across local development and CI.

To build and run the docker container:

```bash
$ make docker
$ make docker-run
```

Alternatively, the Gitlab pipeline pushes the latest image to dockerhub:

```bash
$ docker pull rutigs/do:v0.1.0-b449e05
$ docker run --rm -p 3000:3000 -t rutigs/do:v0.1.0-b449e05
```

## Tradeoffs

The following features were cut due to time constraints:
- Auth middleware for checking the user id/pass
- Prom middleware for tracking request latency
- Unit testing for the Prometheus handler
- An e2e test in the CI pipeline actually running the container and making requests against it

## SLOs

The default Prometheus metrics go applications are included and can be found at `/metrics`. A basic counter metric was added as well and can be seen [here](https://gitlab.com/rutigs/do/-/blob/master/pkg/prometheus/prometheus.go#L12).

Given the features of this application the basis of SLOs could center around the following facets:
- Error rates (eg HTTP 500s, HTTP 200s/400s with invalid response data) would be a useful reliability SLO in that it captures cases where the service can be seen as "unreliable". By capturing this as rates we can levy the errors against different time frames for different end results - for example if error rate in the past month is above the average past year we should allocate more time to hardening the application and improving its reliability.
- Request latency can be good to work into a SLO around what an expected baseline user experience should be. When latency changes and degrades it can point to needed investment in the performance space as service consumers/users can begin to be impacted in negative ways.
