package main

import (
	"encoding/json"
	"log"
	"os"
	"os/signal"
	"strconv"

	"gitlab.com/rutigs/do/pkg/echoer"
	"gitlab.com/rutigs/do/pkg/prometheus"
	"gitlab.com/rutigs/do/pkg/version"

	adaptor "github.com/gofiber/adaptor/v2"
	fiber "github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
)

func main() {
	log.Printf("Echo Server: %s-%s", version.Version, version.GitCommit)

	// call to setupServer to setup routes and handlers
	// this is done via this method instead of directly in main
	// so it can be recreated identically for testing
	app := setupServer()

	// Setup signal ch for graceful shutdown
	shutdownSignalCh := make(chan os.Signal, 1)
	signal.Notify(shutdownSignalCh, os.Interrupt)

	go func() {
		_ = <-shutdownSignalCh
		log.Println("Shutting down the service...")
		_ = app.Shutdown()
	}()

	log.Fatal(app.Listen(":3000"))
}

// setupServer - setup routes, handlers, and middleware for server
func setupServer() *fiber.App {
	// application instance
	app := fiber.New()
	app.Use(logger.New())

	// api group
	api := app.Group("/api")

	// register /echo for PUT/POST methods only
	api.Post("/echo", echoHandler)
	api.Put("/echo", echoHandler)

	// prometheus metrics
	app.Get("/metrics", adaptor.HTTPHandler(prometheus.PrometheusHandler()))

	// Register the 404 last as a catch all for everything not the previous 2 routes
	app.Use(func(c *fiber.Ctx) error {
		return c.SendStatus(404)
	})

	return app
}

// echoHandler - main echo handler for the server
func echoHandler(c *fiber.Ctx) error {
	c.Accepts("application/json")

	jsonBody := c.Body()

	// return HTTP 400 Bad Request if the json is invalid
	if !json.Valid(jsonBody) {
		prometheus.EchoCount.WithLabelValues(strconv.Itoa(400)).Inc()
		return c.SendStatus(400)
	}

	jsonStr := string(jsonBody)

	// return HTTP 400 Bad Request if the json is already echoed
	if echoer.ContainsEchoed(jsonStr) {
		prometheus.EchoCount.WithLabelValues(strconv.Itoa(400)).Inc()
		return c.SendStatus(400)
	}

	// return HTTP 200 with echoed json
	prometheus.EchoCount.WithLabelValues(strconv.Itoa(200)).Inc()
	return c.SendString(echoer.AddEchoed(jsonStr))
}
