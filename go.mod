module gitlab.com/rutigs/do

go 1.16

require (
	github.com/andybalholm/brotli v1.0.3 // indirect
	github.com/gofiber/adaptor/v2 v2.1.10
	github.com/gofiber/fiber/v2 v2.17.0
	github.com/prometheus/client_golang v1.11.0
	github.com/tidwall/gjson v1.8.1
	github.com/tidwall/pretty v1.2.0 // indirect
	github.com/tidwall/sjson v1.1.7
	github.com/valyala/fasthttp v1.29.0 // indirect
	golang.org/x/sys v0.0.0-20210823070655-63515b42dcdf // indirect
)
