FROM golang:1.16.6 as builder

WORKDIR /workspace

COPY go.mod .
COPY go.sum .

# Run this step separate from explicit build so the dependencies can be cached
# in an earlier layer than the code changes
RUN go mod download
COPY . .

RUN make linux

FROM gcr.io/distroless/static:nonroot
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /workspace/bin/do .

ENTRYPOINT ["./do"]
